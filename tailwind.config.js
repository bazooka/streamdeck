let typography = require('@tailwindcss/typography');
let forms = require('@tailwindcss/forms');
let aspectRatio = require('@tailwindcss/aspect-ratio');
const colors = require("./config/colors");
const headings = require("./config/headings");

module.exports = {
    purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {
            fontFamily: {
                'ibm': ['"IBM Plex Sans"', 'sans-serif'],
                'dystopian': ['"Dystopian"', 'sans-serif'],
                'monument': ['"Monument"', 'sans-serif']
            },
            colors,
            animation: {
                'new-chat-message': 'new-chat-message .6s cubic-bezier(.38,0,.25,1)',
                'new-chat-message-paragraph': 'new-chat-message-parapgraph .6s cubic-bezier(.38,0,.25,1) .3s',
                'new-chat-message-author': 'new-chat-message-author .6s cubic-bezier(.38,0,.25,1) .6s',
            },
            keyframes: {
                'new-chat-message': {
                    '0%': {
                        transform: 'translateY(15px) '
                    },
                    '100%': {
                        transform: 'translateY(0)'
                    },
                },
                'new-chat-message-paragraphe': {
                    '0%': {
                        opacity: 0,
                        transform: 'scale(.6)'
                    },
                    '100%': {
                        opacity: 1,
                        transform: 'scale(1)'
                    },
                },
                'new-chat-message-author': {
                    '0%': {
                        opacity: 0,
                        transform: 'translateY(15px)'
                    },
                    '100%': {
                        opacity: 1,
                        transform: 'translateY(0)'
                    },
                }
            }
        },
    },
    variants: {
        extend: {},
    },
    plugins: [
        typography,
        forms,
        aspectRatio,
        headings
    ],
}
