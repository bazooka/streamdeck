import {useEffect, useState} from "react";
import {ChatClient} from "@twurple/chat";
import authProvider from "../utils/twitch/refreshing-auth-provider";

function useTwitchChatClient(channel){
    const [chatClient, setChatClient] = useState(null);

    useEffect(() => {

        if (!!chatClient) {
            return
        }

        async function initChatClient() {

            setChatClient(new ChatClient({authProvider, channels: [channel]}));
        }

        initChatClient();

    }, []);

    useEffect(() => {
        if (!chatClient) {
            return;
        }

        chatClient.connect();

        return () => {
            chatClient.quit();
        }
    }, [chatClient]);

    return {
        chatClient
    }
}

export default useTwitchChatClient;
