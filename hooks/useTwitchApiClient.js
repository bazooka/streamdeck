import {useEffect, useState} from "react";
import {ChatClient} from "@twurple/chat";
import authProvider from "../utils/twitch/refreshing-auth-provider";
import {ApiClient} from "@twurple/api";

function useTwitchApiClient(channel){
    const [apiClient, setApiClient] = useState(null);

    useEffect(() => {

        if (!!apiClient) {
            return
        }

        async function initApiClient() {

            setApiClient(new ApiClient({authProvider, logger: {
                    minLevel: 'debug'
                }}));
        }

        initApiClient();

    }, []);

    return {
        apiClient
    }
}

export default useTwitchApiClient;
