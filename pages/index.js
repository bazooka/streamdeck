import Head from 'next/head'
import Link from 'next/link'
import {useEffect, useState, Fragment} from "react";
import OBSWebsocket from "obs-websocket-js";
import classNames from "classnames";
import {Transition, Dialog} from "@headlessui/react";
import ScenesList from "../components/ScenesList";
import FantassinLogo from "../components/FantassinLogo";
import Header from "../components/Header";

export default function Home() {
    // let [isLoggedWithOBS, setIsLoggedWithOBS] = useState(false)
    let [isLoggedInWithObs, setIsLoggedInWithObs] = useState(false)
    let [isLive, setIsLive] = useState(false)
    let [scenes, setScenes] = useState([]);
    let [obs, setObs] = useState(null);
    let [isModalOpen, setIsLoginWithObs] = useState(true);
    let [isPasswordHidden, setIsPasswordHidden] = useState(true);

    useEffect(() => {
        let obsConnection = new OBSWebsocket();
        setObs(obsConnection);
    }, []);

    // useEffect(() => {
    //
    //     if (!obs) {
    //         return;
    //     }
    //
    //     logInWithObs();
    //
    // }, [obs]);


    useEffect(() => {

        if (!obs) {
            return;
        }

        return async () => {
            await obs.disconnect()
        }
    });

    function handleIsLive() {
        setIsLive(!isLive);
    }

    async function logInWithObs(e) {
        e.preventDefault();
        let formData = new FormData(e.target);
        let address = `${formData.get('host')}:${formData.get('port')}`
        let password = `${formData.get('password')}`
        try {
            await obs.connect({address, password});
            obs.on('ConnectionClosed', (data) => {
                console.log({data})
                setIsLoginWithObs(true)
            });
            obs.on('error', () => {
                setIsLoginWithObs(true)
            });
            let data = await obs.send("GetSceneList");
            setScenes(data.scenes);
            setIsLoginWithObs(false);
        } catch (response) {
            console.error(response.error)
        }
    }

    return (
        <>
            <Head>
                <title>Streamdeck Fantassin</title>
            </Head>

            <Header/>

            <main className="min-h-full min-w-full p-16 grid gap-y-16 main">

                <div className="flex items-start gap-x-16">

                    <div className="flex items-center justify-center w-2/3">

                        {scenes.length === 0 && (
                            <form method="POST"
                                  className="flex flex-col gap-y-2 w-full"
                                  onSubmit={logInWithObs}>
                                <h3 className="mb-8">Se connecter à OBS</h3>
                                <div className="flex gap-8">
                                    <div className="grid gap-y-1 w-full">
                                        <label htmlFor="obs-host">Adresse</label>
                                        <input id="obs-host" type="text" name="host" defaultValue="localhost"/>
                                    </div>
                                    <div className="grid gap-y-1 w-full">
                                        <label htmlFor="obs-port">Port</label>
                                        <input id="obs-port" type="text" name="port" defaultValue="4444"/>
                                    </div>
                                </div>
                                <div className="grid gap-y-1 mb-3">
                                    <label htmlFor="obs-password">Mot de passe</label>
                                    <div className="relative">
                                        <input className="w-full"
                                               id="obs-password"
                                               type={isPasswordHidden ? 'password' : 'text'}
                                               name="password"
                                        />
                                        <button type="button"
                                                className="absolute z-10 inline-flex items-center px-2 right-0 h-full text-black"
                                                onClick={(e) => {
                                                    setIsPasswordHidden(!isPasswordHidden);
                                                }}>
                                            {isPasswordHidden && (
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6"
                                                     fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round"
                                                          strokeWidth={2}
                                                          d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"/>
                                                    <path strokeLinecap="round" strokeLinejoin="round"
                                                          strokeWidth={2}
                                                          d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"/>
                                                </svg>
                                            )}
                                            {!isPasswordHidden && (
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6"
                                                     fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                                    <path strokeLinecap="round" strokeLinejoin="round"
                                                          strokeWidth={2}
                                                          d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"/>
                                                </svg>
                                            )}
                                            <span className="hidden">Afficher/Cacher le champs de mot de passe</span>
                                        </button>
                                    </div>
                                </div>
                                <button
                                    type="submit"
                                    className="inline-flex items-center justify-center px-4 py-3 border border-transparent font-medium text-white font-dystopian font-bold tracking-widest uppercase bg-red hover:bg-red transition duration-500 ease-in-out focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-black focus:ring-red"
                                    onClick={handleIsLive}
                                >
                                    Se connecter à OBS
                                </button>
                            </form>
                            // <button
                            //     type="submit"
                            //     className="inline-flex items-center justify-center px-4 py-3 border border-transparent rounded-md font-medium text-white bg-red-500 hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-white-800 focus:ring-red-500"
                            //     onClick={() => {
                            //         setIsLoginWithObs(true)
                            //     }}
                            // >
                            //     Se connecter à OBS
                            // </button>
                        )}

                        {scenes.length > 0 && (
                            <ScenesList scenes={scenes} obs={obs}/>
                        )}
                    </div>

                    <aside className="w-1/3 bg-gray-900 p-10">
                        <h3 className="mb-8">Se connecter à Twitch</h3>
                        <div className="flex flex-col text-white">
                            Chatbox
                            <button
                                type="submit"
                                className="inline-flex items-center justify-center px-4 py-3 border border-transparent font-medium text-white font-dystopian font-bold tracking-widest uppercase bg-twitch hover:bg-twitch transition duration-500 ease-in-out focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-900 focus:ring-twitch"
                                onClick={handleIsLive}
                            >
                                Se connecter à Twitch
                            </button>
                        </div>
                    </aside>
                </div>
                {/*{isModalOpen && (*/}
                {/*    <Transition appear show={isModalOpen} as={Fragment}>*/}
                {/*        <Dialog*/}
                {/*            as="div"*/}
                {/*            className="fixed inset-0 z-10 overflow-y-auto"*/}
                {/*            onClose={closeModal}*/}
                {/*        >*/}
                {/*            <div className="min-h-screen px-4 text-center">*/}
                {/*                <Transition.Child*/}
                {/*                    as={Fragment}*/}
                {/*                    enter="ease-out duration-300"*/}
                {/*                    enterFrom="opacity-0"*/}
                {/*                    enterTo="opacity-100"*/}
                {/*                    leave="ease-in duration-200"*/}
                {/*                    leaveFrom="opacity-100"*/}
                {/*                    leaveTo="opacity-0"*/}
                {/*                >*/}
                {/*                    <Dialog.Overlay className="fixed inset-0 bg-black opacity-30 blur"/>*/}
                {/*                </Transition.Child>*/}

                {/*                /!* This element is to trick the browser into centering the modal contents. *!/*/}
                {/*                <span*/}
                {/*                    className="inline-block h-screen align-middle"*/}
                {/*                    aria-hidden="true"*/}
                {/*                >*/}
                {/*                  &#8203;*/}
                {/*                </span>*/}
                {/*                <Transition.Child*/}
                {/*                    as={Fragment}*/}
                {/*                    enter="ease-out duration-300"*/}
                {/*                    enterFrom="opacity-0 scale-95"*/}
                {/*                    enterTo="opacity-100 scale-100"*/}
                {/*                    leave="ease-in duration-200"*/}
                {/*                    leaveFrom="opacity-100 scale-100"*/}
                {/*                    leaveTo="opacity-0 scale-95"*/}
                {/*                >*/}
                {/*                    <div*/}
                {/*                        className="inline-block w-full max-w-md p-6 my-8 overflow-hidden text-left align-middle transition-all transform bg-white shadow-xl rounded-lg">*/}
                {/*                        <Dialog.Title*/}
                {/*                            as="h3"*/}
                {/*                            className="text-lg font-bold leading-6 text-gray-900"*/}
                {/*                        >*/}
                {/*                            Se connecter à OBS*/}
                {/*                        </Dialog.Title>*/}

                {/*                    </div>*/}
                {/*                </Transition.Child>*/}
                {/*            </div>*/}
                {/*        </Dialog>*/}
                {/*    </Transition>*/}
                {/*)}*/}
            </main>
        </>
    )
}
