import Head from "next/head";
import Link from "next/link";
import Header from "../../components/Header";

export default function Overlays() {

    let overlays = [
        {
            name: 'Pre-Intro',
            description: '',
            link: 'pre-intro'
        },
        {
            name: 'Intro',
            description: '',
            link: 'intro'
        },
        {
            name: 'Pause Kit-Kat',
            description: '',
            link: 'pause-kit-kat'
        },
        {
            name: 'Questions Time',
            description: '',
            link: 'questions'
        },
        {
            name: 'End',
            description: '',
            link: 'end'
        }
    ]

    return (
        <>
            <Head>
                <title>Overlays — Archive</title>
            </Head>

            <Header/>

            <main
                className="grid grid-cols-2 auto-rows-min gap-px min-h-full min-w-full items-start justify-start p-16 relative rounded-lg overflow-hidden divide-y items-start justify-start divide-gray-200 sm:divide-y-0 main">
                <h1 className="mb-8">Liste des overlays</h1>
                <div className="empty"></div>

                {overlays?.length > 0 && overlays.map((overlay, index) => (
                    <div key={index}
                         className="flex items-center gap-x-4 relative group bg-gray-900 p-6 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-500">
                        <span className="inline-flex items-center justify-center rounded-full p-3 bg-gray-800 w-8 h-8">{index}</span>
                        <div>
                            <h3 className="text-lg font-medium">
                                <Link href={`/overlay/${overlay.link}`}>
                                    <a className="focus:outline-none">
                                        <span className="absolute inset-0" aria-hidden="true"></span>
                                        {overlay.name}
                                    </a>
                                </Link>
                            </h3>
                            {overlay.description && (
                                <p className="mt-2 text-sm text-gray-500">
                                    {overlay.description}
                                </p>
                            )}
                        </div>
                        <span
                            className="pointer-events-none absolute top-6 right-6 text-gray-300 group-hover:text-gray-400"
                            aria-hidden="true"
                        >
                          <svg className="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24">
                            <path
                                d="M20 4h1a1 1 0 00-1-1v1zm-1 12a1 1 0 102 0h-2zM8 3a1 1 0 000 2V3zM3.293 19.293a1 1 0 101.414 1.414l-1.414-1.414zM19 4v12h2V4h-2zm1-1H8v2h12V3zm-.707.293l-16 16 1.414 1.414 16-16-1.414-1.414z"/>
                          </svg>
                        </span>
                    </div>
                ))}

            </main>
        </>
    )
}
