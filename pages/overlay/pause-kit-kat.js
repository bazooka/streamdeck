import Head from "next/head";
import {gsap} from "gsap";
import {useEffect, useRef} from "react";
import Camera from "../../components/Camera";
import TwitchChat from "../../components/TwitchChat";

export default function Intro() {

    let tl = gsap.timeline({repeat: -1, repeatDelay: 1, yoyo: true});
    let titleRef = useRef();

    useEffect(() => {
        let eachWordsInTitle = titleRef.current.querySelectorAll('span');
        console.log(eachWordsInTitle);
        tl.from(eachWordsInTitle, {
            duration: 1,
            y: "100%",
            stagger: .2,
            ease: 'power4.out'
        })
        // tl.to(eachWordsInTitle, {
        //     duration: 1,
        //     y: "0%",
        //     stagger: .3,
        //     ease: 'power4.out'
        // });
        tl.play();
    });
    return (
        <>
            <Head>
                <title>Overlay — Pause Kit Kat</title>
            </Head>

            <main className="flex gap-x-36 p-36 pb-16 kit-kat main">
                <div className="flex flex-col gap-y-16 justify-between items-center" style={{width: '38%'}}>
                    <Camera/>
                    <Camera/>
                </div>
                <div className="flex-1 flex flex-col gap-y-16 justify-between items-center" style={{width: '62%'}}>
                    <div className="w-full">
                        <h2 ref={titleRef} className="is-style-outline overflow-hidden"><span className="inline-flex">Pause</span> <span className="inline-flex">kit</span><span className="inline-flex">-</span><span className="inline-flex">kat</span></h2>
                        <h2>des questions&nbsp;?</h2>
                    </div>
                    {/*border border-red*/}
                    <div className="h-full w-full">
                        <TwitchChat/>
                    </div>
                </div>
            </main>
        </>
    )
}
