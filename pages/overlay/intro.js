import Head from "next/head";
import {gsap} from "gsap";
import {useEffect, useRef} from "react";
import SocialFooter from "../../components/SocialFooter";
import FantassinLogo from "../../components/FantassinLogo";
import Camera from "../../components/Camera";
import nl2br from "../../utils/nl2br";
import streamMetas from "../../data/stream_metas.json";
import TwitchChat from "../../components/TwitchChat";

export default function Intro() {

    let { title } = streamMetas;
    let tl = gsap.timeline({repeat: -1, repeatDelay: 1, yoyo: true});
    let titleRef = useRef();

    useEffect(() => {
        let eachWordsInTitle = titleRef.current.querySelectorAll('span');
        console.log(eachWordsInTitle);
        tl.from(eachWordsInTitle, {
            duration: 1,
            y: "100%",
            stagger: .2,
            ease: 'power4.out'
        })
        // tl.to(eachWordsInTitle, {
        //     duration: 1,
        //     y: "0%",
        //     stagger: .3,
        //     ease: 'power4.out'
        // });
        tl.play();
    });
    return (
        <>
            <Head>
                <title>Overlay — Intro</title>
            </Head>

            <main className="relative min-h-full min-w-full flex intro gap-x-60 p-36 pb-16 relative main">
                <div className="absolute top-36 left-0 w-full flex flex-wrap justify-between items-center ml-36 pl-16" style={{ left: 104, width: '38%'}}>
                    <Camera/>
                </div>
                <div className="relative w-2/3 flex flex-col justify-end z-10 mt-auto h-full">
                    <div className="flex gap-x-16">
                        <FantassinLogo className="flex-none" size={104}/>
                        <div>
                            <h2 ref={titleRef} className="is-style-outline overflow-hidden mb-8"><span className="inline-flex">Bienvenue</span>&nbsp;<span className="inline-flex">!</span></h2>
                            <h2 className="mb-32" dangerouslySetInnerHTML={{__html: nl2br(title)}}></h2>
                            {/*absolute left-0 bottom-0 w-full h-full left-36 bottom-16*/}
                        </div>
                    </div>

                    <SocialFooter/>
                </div>
                <div className="w-1/3 h-full">
                    <TwitchChat/>
                </div>
            </main>
        </>
    )
}
