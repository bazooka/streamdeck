import Head from "next/head";
import {gsap} from "gsap";
import {useEffect, useRef} from "react";
import Background from "../../components/Background";
import SocialFooter from "../../components/SocialFooter";
import FantassinLogo from "../../components/FantassinLogo";
import LiveAlert from "../../components/LiveAlert";
import nl2br from "../../utils/nl2br";
import useLocalStorage from "../../hooks/useLocalStorage";
import streamMetas from "../../data/stream_metas.json"

export default function PreIntro() {

    let { title } = streamMetas
    let tl = gsap.timeline({repeat: -1, repeatDelay: 1, yoyo: true});
    let titleRef = useRef();

    useEffect(() => {
        let eachWordsInTitle = titleRef.current.querySelectorAll('span');
        console.log(eachWordsInTitle);
        tl.from(eachWordsInTitle, {
            duration: 1,
            y: "100%",
            stagger: .2,
            ease: 'power4.out'
        })
        // tl.to(eachWordsInTitle, {
        //     duration: 1,
        //     y: "0%",
        //     stagger: .3,
        //     ease: 'power4.out'
        // });
        tl.play();
    });
    return (
        <>
            <Head>
                <title>Overlay — Pre-Intro</title>
            </Head>

            <main className="min-h-full min-w-full grid p-36 pb-16 relative main">
                {/*<Background/>*/}
                <div className="relative flex flex-col justify-end z-10 w-full h-full">
                    <div className="absolute top-0 left-0 w-full flex justify-between items-center">
                        <FantassinLogo size={148}/>
                        <LiveAlert title="Live"/>
                    </div>
                    <h2 ref={titleRef} className="is-style-outline overflow-hidden mb-8"><span
                        className="inline-flex">On</span> <span className="inline-flex">arrive</span>&nbsp;<span
                        className="inline-flex">!</span></h2>
                    { title && (
                        <h1 className="mb-32" dangerouslySetInnerHTML={{__html: nl2br(title)}}></h1>
                    )}

                    {/*absolute left-0 bottom-0 w-full h-full left-36 bottom-16*/}
                    <SocialFooter/>
                </div>
            </main>
        </>
    )
}
