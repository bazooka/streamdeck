import Head from "next/head";
import {gsap} from "gsap";
import {useEffect, useRef} from "react";
import SocialFooter from "../../components/SocialFooter";
import FantassinLogo from "../../components/FantassinLogo";
import Camera from "../../components/Camera";
import LiveAlert from "../../components/LiveAlert";

export default function Intro() {

    let tl = gsap.timeline({repeat: -1, repeatDelay: 1, yoyo: true});
    let titleRef = useRef();

    useEffect(() => {
        let eachWordsInTitle = titleRef.current.querySelectorAll('span');
        tl.from(eachWordsInTitle, {
            duration: 1,
            y: "100%",
            stagger: .2,
            ease: 'power4.out'
        })
        tl.play();
    }, []);

    return (
        <>
            <Head>
                <title>Overlay — End</title>
            </Head>

            <main className="min-h-full min-w-full grid intro p-36 pb-16 relative main">
                <div className="relative flex flex-col justify-end z-10 w-full h-full">
                    <div className="flex gap-x-16">
                        <FantassinLogo className="flex-none" size={104}/>
                        <div>
                            <h2 ref={titleRef} className="is-style-outline overflow-hidden mb-8"><span className="inline-flex">Fin</span> <span className="inline-flex">du</span> <span className="inline-flex">Live</span>&nbsp;<span className="inline-flex">!</span></h2>
                            <h2 className="mb-32">Merci pour votre présence, <br/> à bientôt</h2>
                        </div>
                    </div>
                    <SocialFooter/>
                </div>
            </main>
        </>
    )
}
