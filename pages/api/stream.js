import fs from 'fs';

export default function handler(req, res) {

  const path = './data/stream_metas.json';

  // Get data from stream_metas.json file.
  let streamJson = fs.readFileSync(path);
  let streamMetas = JSON.parse(streamJson);
  let { title } = req.body;

  // Merge new data with old data.
  let mergedStreamMetas = {
    ...streamMetas,
    title
  }

  if(! mergedStreamMetas.titles.includes(title)){
    mergedStreamMetas.titles.push(title);
  }

  // Save merged data in file.
  fs.writeFileSync(path, JSON.stringify(mergedStreamMetas, null, "\t"));

  res.status(200).json(mergedStreamMetas)
}
