import fs from 'fs';

export default async function handler(req, res) {
  const path = './data/tokens.json';
  fs.writeFileSync(path, JSON.stringify(req.body, null, "\t"));

  res.status(200).json(req.body)
}
