import Head from 'next/head'
import Header from "../components/Header";
import streamMetas from "../data/stream_metas.json";
import nl2br from "../utils/nl2br";
import {useRef} from "react";
import useTwitchApiClient from "../hooks/useTwitchApiClient";
import { ApiClient } from '@twurple/api';
import authProvider from "../utils/twitch/refreshing-auth-provider";

function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function Home() {
    let {title, titles} = streamMetas;
    let titleRef = useRef(null);
    let formRef = useRef(null);
    const apiClient = new ApiClient({ authProvider });

    async function handleTitleChange(e) {
        e.preventDefault()
        titleRef.current.value = e.target.getAttribute('data-title');
        await saveStreamMetas(e);
    }

    async function saveStreamMetas(e) {
        e.preventDefault();
        let formData = new FormData(formRef.current);
        let title = String(formData.get('stream_title'));

        const user = await apiClient.users.getUserByName('fts_squad');
        await apiClient.channels.updateChannelInfo(user, { title });

        await fetch('/api/stream', {
            method: 'POST',
            body: JSON.stringify({
                title,
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    return (
        <>
            <Head>
                <title>Streamdeck Fantassin</title>
            </Head>

            <Header/>

            <main className="min-h-full min-w-full p-16 grid gap-y-16">
                <div className="flex w-full gap-x-16">
                    <form ref={formRef} className="flex flex-1 flex-col" method="POST" onSubmit={saveStreamMetas}>
                        <label htmlFor="stream_title">Titre du stream</label>
                        <textarea ref={titleRef} id="stream_title" name="stream_title" defaultValue={title}/>
                        <button type="submit">Sauvegarder</button>
                    </form>

                    {titles?.length > 0 && (
                        <div className="flex flex-col flex-1 space-y-1">
                            <h3>Titres déjà utilisés</h3>
                            {titles.map(titleName => {
                                return (
                                    <button
                                        key={titleName}
                                        className={classNames(
                                            title === titleName ? 'bg-gray-100 text-gray-900' : 'text-gray-600 hover:bg-gray-50 hover:text-gray-900',
                                            'group flex justify-start px-3 py-2 text-sm font-medium rounded-md'
                                        )}
                                        onClick={handleTitleChange}
                                        data-title={titleName}
                                        dangerouslySetInnerHTML={{__html: nl2br(titleName)}}
                                    >
                                    </button>
                                )
                            })}
                        </div>
                    )}
                </div>


            </main>
        </>
    )
}
