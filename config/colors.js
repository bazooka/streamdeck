const colors = require('tailwindcss/colors')

module.exports = {
    'transparent': 'transparent',
    'twitch': "#6441a5",
    'red': "#FF4D6A",
    'black': colors.black,
    'white': colors.white,
    'gray': '#777777',
}
