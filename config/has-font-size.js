const _ = require('lodash')
const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({addUtilities, theme}) {
    const hasFontSize = _.map(theme('colors'),(value, key) => {
        return {
            [`.has-${key}-background-color`]: {
                backgroundColor: `${value}`
            }
        }
    })

    addUtilities(hasColorUtilities)
})
