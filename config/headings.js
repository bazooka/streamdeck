// const _ = require('lodash')
const plugin = require('tailwindcss/plugin');

module.exports = plugin(function({ addBase, theme }) {
    const headings = {
        'h1': {
            fontFamily: theme('fontFamily.dystopian'),
            fontSize: "83px",
            lineHeight: 1.17,
            fontWeight: theme('fontWeight.bold'),
            letterSpacing: "3.38px",
            // textTransform: "uppercase",
            margin: 0
        },
        'h2': {
            fontFamily: theme('fontFamily.dystopian'),
            fontSize: "55px",
            lineHeight: 1.12,
            fontWeight: theme('fontWeight.bold'),
            letterSpacing: "3.38px",
            margin: 0
        },
        'h3': {
            fontFamily: theme('fontFamily.dystopian'),
            fontSize: "28px",
            margin: 0
            // textTransform: "uppercase",
        },
        // 'h2': {
        //     fontSize: theme('fontSize.h2'),
        //     fontFamily: theme('fontFamily.miriam'),
        //     fontWeight: theme('fontWeight.bold'),
        // },
    }

    addBase(headings)
})
