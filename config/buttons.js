const _ = require('lodash')
const plugin = require('tailwindcss/plugin');

module.exports = plugin(function ({addComponents, theme}) {
    const buttons = {
        '.wp-block-button__link': {
            display: 'inline-flex',
            fontFamily: theme('fontFamily.miriam'),
            borderRadius: 6,
            padding: '13px 20px',
            backgroundColor: 'var(--button-background-color)',
            color: 'var(--button-color)',
            border: '2px solid var(--button-border-color)',
            '&:hover': {
                backgroundColor: theme('colors.tonic.dark')
            },
            [`${theme('screen.md')}`]: {
                padding: '14px 30px',
            }
        },
    }

    addComponents(buttons)
})
