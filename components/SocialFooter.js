import Image from 'next/image';
import WebLogo from "./WebLogo";
import YouTubeLogo from "./YouTubeLogo";
import TwitterLogo from "./TwitterLogo";
import InstagramLogo from "./InstagramLogo";

export default function SocialFooter({className}) {

    return (
        <div className={`flex gap-x-16 text-xl ${className}`}>
            <div className="flex items-center gap-x-4">
                <WebLogo size={48}/>
                <span>https://learn.fantassin.fr/</span>
            </div>
            <div className="flex items-center gap-x-4">
                <YouTubeLogo size={48}/>
                <span>Fantassin</span>
            </div>
            <div className="flex items-center gap-x-4">
                <TwitterLogo size={48}/>
                <span>fts_squad</span>
            </div>
            <div className="flex items-center gap-x-4">
                <InstagramLogo size={48}/>
                <span>fts_squad</span>
            </div>
        </div>
    )
}
