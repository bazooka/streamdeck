export default function YouTubeLogo({size}) {

    return (
        <svg viewBox="0 0 512 512" height={size} width={size} xmlns="http://www.w3.org/2000/svg">
            <g transform="matrix(21.333333333333332,0,0,21.333333333333332,0,0)">
                <path
                    d="M23.25,7.44A3.44,3.44,0,0,0,19.81,4H4.19A3.44,3.44,0,0,0,.75,7.44v9.12A3.44,3.44,0,0,0,4.19,20H19.81a3.44,3.44,0,0,0,3.44-3.44ZM9.5,15.94V7.28l6.77,4.33Z"
                    fill="currentColor"
                    // stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="1.5"/>
            </g>
        </svg>
    )
}
