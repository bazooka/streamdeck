export default function InstagramLogo({size}) {

    return (
        <svg height={size} width={size} viewBox="0 0 512 512" fill="none">
            <path
                d="M391.68 16H120.32C62.7057 16 16 62.7057 16 120.32V391.68C16 449.294 62.7057 496 120.32 496H391.68C449.294 496 496 449.294 496 391.68V120.32C496 62.7057 449.294 16 391.68 16Z"
                stroke="currentColor"
                strokeWidth="32"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M256 361C313.99 361 361 313.99 361 256C361 198.01 313.99 151 256 151C198.01 151 151 198.01 151 256C151 313.99 198.01 361 256 361Z"
                stroke="currentColor"
                strokeWidth="32"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
            <path
                d="M395 129C401.075 129 406 123.851 406 117.5C406 111.149 401.075 106 395 106C388.925 106 384 111.149 384 117.5C384 123.851 388.925 129 395 129Z"
                stroke="currentColor"
                strokeWidth="32"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </svg>
    )
}
