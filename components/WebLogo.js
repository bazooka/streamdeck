export default function InstagramLogo({size}) {

    return (
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height={size} width={size}>

            <g transform="matrix(21.333333333333332,0,0,21.333333333333332,0,0)">
                <g>
                    <g>
                        <path
                            d="M13.59,23.14a11.9,11.9,0,0,1-1.59.11A11.25,11.25,0,1,1,23.15,10.47a11.63,11.63,0,0,1,0,3.12"
                            fill="none"
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="1.5"
                        />
                        <path
                            d="M9.29,22.92C7.77,20.69,6.75,16.63,6.75,12s1-8.69,2.54-10.92"
                            fill="none"
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="1.5"
                        />
                        <line x1="0.77" y1="11.25" x2="11.25" y2="11.25"
                              fill="none"
                              stroke="currentColor"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="1.5"
                        />
                        <line x1="16.5" y1="11.25" x2="23.23" y2="11.25"
                              fill="none"
                              stroke="currentColor"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="1.5"
                        />
                        <line x1="3" y1="5.25" x2="21" y2="5.25"
                              fill="none"
                              stroke="currentColor"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="1.5"
                        />
                        <line x1="2.05" y1="17.25" x2="10.5" y2="17.25"
                              fill="none"
                              stroke="currentColor"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="1.5"
                        />
                        <path d="M14.71,1.08c1.8,2.64,2.45,7,2.53,10.17"
                              fill="none"
                              stroke="currentColor"
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              strokeWidth="1.5"
                        />
                    </g>
                    <path
                        d="M13,13.85,15.17,21a.67.67,0,0,0,1.11.28l1.1-1.1,2.84,2.83a.65.65,0,0,0,.94,0l1.89-1.89a.66.66,0,0,0,0-.94l-2.83-2.84,1.1-1.1A.67.67,0,0,0,21,15.17L13.85,13A.68.68,0,0,0,13,13.85Z"
                        fill="none"
                        stroke="currentColor"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.5"
                    />
                </g>
            </g>
        </svg>
    )
}
