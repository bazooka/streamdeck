import FantassinLogo from "./FantassinLogo";
import Link from "next/link";

export default function Header({className}){
    return (
        <header className="flex justify-between p-16">
            <Link href="/">
                <a className="flex flex-none gap-x-4 flex-1 min-w-0">
                    <FantassinLogo className="flex-none" size={44}/>
                    <h3>Streamdeck</h3>
                </a>
            </Link>

            <nav>
                <ul className="flex items-center gap-x-4">
                    <li>
                        <Link href="/overlay">
                            <a>
                                Overlays
                            </a>
                        </Link>
                    </li>
                    <li>
                        <Link href="/stream-settings">
                            <a className="inline-flex items-center justify-center px-4 py-3 border border-transparent font-medium text-white font-dystopian font-bold tracking-widest uppercase bg-twitch hover:bg-twitch transition duration-500 ease-in-out focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-900 focus:ring-twitch">
                                Configurer le stream
                            </a>
                        </Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}
