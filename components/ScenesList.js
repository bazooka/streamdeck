import classNames from "classnames";
import {useEffect, useRef, useState} from "react";
import Image from 'next/image'


export default function ScenesList({scenes, obs}) {

    let [currentScene, setCurrentScene] = useState(null);
    let [currentSceneImage, setCurrentSceneImage] = useState(null);
    const requestRef = useRef();

    async function handleSetCurrentScene(scene) {
        try {
            await obs.send("SetCurrentScene", {"scene-name": scene.name});
            takeSourceScreenshot(scene.name)
            // await takeSourceScreenshot(scene.name);
            // setCurrentSceneImage( requestAnimationFrame(takeSourceScreenshot) );
        } catch (response) {
            console.error(response.error)
        }
    }

    async function takeSourceScreenshot(sourceName) {
        let data = await obs.send("TakeSourceScreenshot", {
            sourceName,
            embedPictureFormat: 'png',
            width: 1280,
            height: 720
        });
        setCurrentSceneImage(data.img);
    }

    return (
        scenes.length > 0 && (
            <div className="flex items-start gap-16 w-full">

                <div className="flex flex-col gap-y-4 w-1/6">
                    <h3>Scènes disponibles</h3>
                    <ul
                        role="list"
                        className="flex flex-col gap-y-8"
                    >
                        {scenes.map((scene) => (
                            <li key={scene.name} className="relative aspect-w-16 aspect-h-9">
                                <button
                                    className={
                                        classNames(
                                            false
                                                ? 'ring-2 ring-offset-2 ring-indigo-500'
                                                : 'focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500',
                                            'group flex items-center justify-center rounded-lg bg-gray-100 overflow-hidden outline-none group-hover:opacity-75'
                                        )
                                    }
                                    onClick={() => {
                                        handleSetCurrentScene(scene)
                                    }}
                                >
                                    {/*<img*/}
                                    {/*    src={scene.source}*/}
                                    {/*    alt=""*/}
                                    {/*    className={classNames(*/}
                                    {/*        scene.current ? '' : 'group-hover:opacity-75',*/}
                                    {/*        'object-cover pointer-events-none'*/}
                                    {/*    )}*/}
                                    {/*/>*/}
                                    <span
                                        className="block text-sm font-medium text-gray-900 truncate pointer-events-none">
                                            {scene.name}
                                        </span>
                                </button>
                                {/*<p className="block text-sm font-medium text-gray-500 pointer-events-none">{scene.size}</p>*/}
                            </li>
                        ))}
                    </ul>
                </div>

                <div className="flex-1">
                    {currentSceneImage && (
                        <Image className="border h-auto w-full"
                               src={currentSceneImage}
                               alt="Current Scene displayed on OBS"
                               width={1280}
                               height={720}
                        />
                    )}
                    {!currentSceneImage && (
                        <span>Aucune scène n&apos;est active pour le moment…</span>
                    )}
                </div>
            </div>
        )
    )
}
