import Image from 'next/image';
import {Transition} from "@headlessui/react";
import {useEffect, useRef} from "react";
import {gsap} from "gsap";

export default function Message({message}){
    const messageContainer = useRef(null)

    useEffect(() => {

        let tl = gsap.timeline()
        tl.addLabel('start')

        tl.to(messageContainer.current.querySelector('blockquote'), {
            duration: .4,
            autoAlpha: 1,
            scale: 1,
            ease: "power4.inOut"
        })

        tl.to(messageContainer.current, {
            duration: .3,
            y: 0,
            ease: "power4.out"
        }, 'start+=.3')

        tl.to(messageContainer.current.querySelector('cite'), {
            duration: .4,
            autoAlpha: 1,
            y: 0,
            ease: "power4.out"
        }, 'start+=.6')

        tl.to(messageContainer.current.querySelector('p'), {
            duration: .4,
            autoAlpha: 1,
            ease: "power4.out"
        }, 'start+=.5')

        return() => {
            tl.reverse()
            tl.kill();
        }

    }, []);

    return (
        <div ref={messageContainer} className="transform translate-y-8">
            <blockquote className="transform scale-50 opacity-0 origin-bottom-left flex flex-col font-ibm bg-white rounded-lg rounded-bl-none text-black text-h4 p-4">
                <p className="space-x-1 opacity-0">{message.parseEmotes().map(messagePart => {
                    switch (messagePart.type) {
                        case 'emote':
                            return (
                                <Image
                                    layout="fill"
                                    key={messagePart.id}
                                    src={`https://static-cdn.jtvnw.net/emoticons/v2/${messagePart.id}/default/dark/3.0`}
                                    alt={messagePart.name}
                                    className="inline-block w-8 h-8"
                                />
                            );
                        case 'text':
                            return (
                                <span key={messagePart.position}>
                                    {messagePart.text}
                                </span>
                            );
                    }
                })}</p>
            </blockquote>
            <cite className="relative block transform translate-y-4 opacity-0 mt-1 text-small text-gray font-dystopian font-bold font-normal not-italic">
                {message.userInfo.displayName}
                {/*<div className="-space-x-4 absolute top-0 right-2 flex">*/}
                {/*{message.userInfo.isBroadcaster && (*/}
                {/*    <span className="relative flex items-center justify-center rounded-full bg-red w-8 h-8 border-black border-2 block transform -translate-y-1/2 mr-2">*/}
                {/*        <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="currentColor"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M15 8v8H5V8h10m1-2H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.55 0 1-.45 1-1v-3.5l4 4v-11l-4 4V7c0-.55-.45-1-1-1z"/></svg>*/}
                {/*    </span>*/}
                {/*)}*/}
                {/*{message.userInfo.isMod && (*/}
                {/*    <span className="relative flex items-center justify-center rounded-full bg-white w-8 h-8 border-black border-2 block transform -translate-y-1/2 mr-2">*/}
                {/*        <svg class="w-5 h-5 text-black" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="currentColor"><rect fill="none" height="24" width="24"/><path d="M14.5,12.59l0.9,3.88L12,14.42l-3.4,2.05l0.9-3.87l-3-2.59l3.96-0.34L12,6.02l1.54,3.64L17.5,10L14.5,12.59z M12,3.19 l7,3.11V11c0,4.52-2.98,8.69-7,9.93C7.98,19.69,5,15.52,5,11V6.3L12,3.19 M12,1L3,5v6c0,5.55,3.84,10.74,9,12c5.16-1.26,9-6.45,9-12 V5L12,1L12,1z"/></svg>*/}
                {/*    </span>*/}
                {/*)}*/}
                {/*</div>*/}
            </cite>
        </div>
    )
}
