import Image from 'next/image';

export default function LiveAlert({className, title, description}){
    return (
        <div className={`inline-flex items-center bg-red text-white gap-x-4 px-8 py-6 ${className}`}>
            <div className="relative transform translate-y-1">
                <span className="absolute l-0 right-0 animate-ping inline-flex bg-white rounded-full" style={{width: '26px', height: '26px'}}></span>
                <span className="inline-flex bg-white rounded-full" style={{width: '26px', height: '26px'}}></span>
            </div>
            { title  && (
                <h3 className="inline-flex uppercase font-black leading-none tracking-widest">{title}</h3>
            )}
            { description  && (
                <span className="font-ibm">{description}</span>
            )}
        </div>
    )
}
