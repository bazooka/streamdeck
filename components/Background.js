import Image from 'next/image';

export default function Background({className}){
    return (
        <Image
            className={className}
            alt="Background"
            src="/mountains.jpg"
            layout="fill"
            objectFit="cover"
            quality={100}
        />
    )
}
