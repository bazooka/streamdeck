import {useEffect, useReducer} from "react";
import Message from "./Message";
import useTwitchChatClient from "../hooks/useTwitchChatClient";

function reducer(messages, newMessage) {
    const limit = 5;
    let messagesVisibles = messages;
    newMessage.isVisible = true;

    // if (messages.length >= limit) {
    //     messagesVisibles = messages.slice(-1 * limit);
    //     messagesVisibles[0].isVisible = false;
    // }

    // console.log({messagesVisibles})

    return [...messagesVisibles, newMessage]
}

export default function TwitchChat() {

    const [messages, setMessages] = useReducer(reducer, []);
    const { chatClient } = useTwitchChatClient('fts_squad');

    const onMessageHandler = (channel, user, message, msg) => {
        // Do something with the message
        setMessages(msg);
    };

    useEffect(() => {
        if (!chatClient) {
            return
        }

        /**
         * TODO: Hot reload from Next.js multiply messages.
         */
        chatClient.onMessage(onMessageHandler);

    }, [chatClient]);

    return (
        <div className="space-y-6">
            { messages.map((message) => (<Message key={message.id} message={message}/>)) }
        </div>
    )
}
