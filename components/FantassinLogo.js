export default function FantassinLogo({size, className}){
    return (
        <svg
            x="0px"
            y="0px"
            width={ size }
            height={ size }
            viewBox="0 0 148.7 148.7"
            xmlSpace="preserve"
            className={className}
        >
            <circle
                className="circle"
                stroke="currentColor"
                strokeWidth="7"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="transparent"
                cx="74.4"
                cy="74.4"
                r="70.9"
            ></circle>
            <path
                className="f"
                stroke="currentColor"
                strokeWidth="7"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="transparent"
                d="M75.5,21.7l-6.9,2.8c-1.9,0.8-3.3,2.4-3.7,4.4l-20.7,89.4L32.6,88.4c-1.2-3.2,0.3-6.8,3.5-8.1l82.1-33.2"
            ></path>
            <line
                className="t"
                stroke="currentColor"
                strokeWidth="7"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="transparent"
                x1="81.4"
                y1="47.1"
                x2="70.9"
                y2="92.5"
            ></line>
            <path
                className="s"
                stroke="currentColor"
                strokeWidth="7"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="transparent"
                d="M118.3,47.1l-13,5.2c-4.7,1.9-5.3,8.4-1.1,11.1l3.9,2.5c4.3,2.8,3.7,9.2-1.1,11.1l-13,5.2"
            ></path>
        </svg>
    )
}
