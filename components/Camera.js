import {useEffect, useRef} from 'react';

export default function Camera({className, width}) {
    const cameraVideo = useRef(null);

    /**
     * Fonctionne mais n'est pas affiché dans OBS.
     * La ref. "cameraVideo" doit être une balise <video autoPlay>.
     */
    // const constraints = {
    //     video: {
    //         width: {ideal: 1280},
    //         height: {ideal: 720}
    //     }
    // };
    //
    // let stream = null;
    // useEffect(() => {
    //     async function getMedia(){
    //
    //         try {
    //             stream = await navigator.mediaDevices.getUserMedia(constraints);
    //             cameraVideo.current.srcObject = stream;
    //         } catch(err) {
    //             /* handle the error */
    //         }
    //     }
    //
    //     getMedia();
    // }, []);

    return (
        // border border-red
        <div ref={cameraVideo} className="flex aspect-w-16 aspect-h-9 w-full">
        </div>
    )
}
