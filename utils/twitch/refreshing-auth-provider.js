import {RefreshingAuthProvider} from "@twurple/auth";
import tokenData from "../../data/tokens.json";

const authProvider = new RefreshingAuthProvider(
    {
        clientId: process.env.NEXT_PUBLIC_TWITCH_CLIENT_ID,
        clientSecret: process.env.NEXT_PUBLIC_TWITCH_CLIENT_SECRET,
        currentScopes: [
            "whispers:read",
            "whispers:edit",
            "channel:moderate",
            "chat:edit",
            "chat:read",
            "channel:manage:broadcast",
            "channel:manage:polls",
            "channel:manage:predictions",
            "channel:manage:redemptions",
            "channel:manage:schedule",
            "channel:read:goals",
            "channel:read:hype_train",
            "channel:read:polls",
            "channel:read:predictions",
            "channel:read:redemptions",
            "channel:read:subscriptions",
            "user:edit",
            "user:read:follows",
            "user:read:subscriptions"
        ],
        onRefresh: async (newTokenData) => {
            const response = await fetch('/api/twitch-save-tokens', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(newTokenData)
            });
            return await response.json();
        }
    },
    tokenData
);

export default authProvider;
